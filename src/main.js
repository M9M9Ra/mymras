import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import PrimeVue from 'primevue/config';
import "primevue/resources/themes/lara-light-teal/theme.css"/*светлая зелёная тема */
import "primevue/resources/primevue.min.css";
import "primeicons/primeicons.css";


import components from './UI'

const app = createApp(App)

components.forEach(element => {
    app.component(element.name, element)
})

app
    .use(router)
    .use(PrimeVue, { ripple: true })
    .mount('#app')
