import BurgMenu from "@/UI/BurgMenu.vue";
import BurgButton from "@/UI/BurgButton.vue";
import Modal from "@/UI/Modal.vue";
import NavButton from "@/UI/NavButton.vue";
import SelectList from "@/UI/SelectList.vue";
import UserItem from "@/UI/UserItem.vue";
import User from "@/UI/User.vue";

export default [
    BurgMenu, BurgButton, Modal, NavButton, SelectList, UserItem, User
]