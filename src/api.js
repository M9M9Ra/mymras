import axios from "axios";

let subscribe = async (message, roomId) => {
    const eventSource = new EventSource(`http://localhost:5000/connection/${roomId}`)
    eventSource.onopen = () => {
        console.log("Open")
    }
    eventSource.onmessage =  (events) => {
        console.log("Message")
        let messageData = JSON.parse(events.data)
        message.value.push(messageData)
    }
    eventSource.onerror = (events) => {
        console.log("Error")
    }
}
let sendMessage = async (userName, message, roomId) => {
  await axios.post(`http://localhost:5000/send-message/${roomId}`, {
      message: message,
      id : userName
  })
}

let createRoom = async (buyer, salesman) => {
    let data = await axios.get(`http://localhost:5000/create-room/${buyer}`, {
        buyer : buyer,
        salesman : salesman
    })
    console.log(data.data)
    return data.data
}

export {subscribe, sendMessage, createRoom}