import { createRouter, createWebHashHistory, createWebHistory } from 'vue-router'
import calculator from "@/pages/calculator.vue";
import database from "@/pages/database.vue";
import base from "@/pages/Base.vue";
import MainPage from "@/pages/MainPage.vue";

const routes = [
  {
    path: '/',
    component: MainPage
  },
  {
    path: '/calculator',
    component: calculator
  },
  {
    path: '/database',
    component: database
  },
  {
    path: '/base',
    component: base
  },
  {
    path: '/chat',
    component: () => import("@/pages/Chat.vue")
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router
