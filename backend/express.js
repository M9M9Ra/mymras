const express = require("express");
const { json } = require("body-parser");
const mysql = require("mysql2");
const cors = require('cors')
const jsonParser = express.json();
let app = express().use(cors());

let dbAccount = {
  connectionLimit: 5,
  host: "localhost",
  user: "root",
  database: 'usersdb2',
  password: "root"
}

const pool = mysql.createPool(dbAccount)


//Первичное получение всех пользователей
app.post("/", jsonParser, function (request, response) {
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Headers', 'origin, content-type, accept');

    pool.query("SELECT * FROM `persons`", function (err, data) {
        if (err) {
            console.error(err.message)
        }
        else {
            console.log(JSON.stringify(data))
            response.send(data)
        }
    })
});

//Отправка SQL запросов напрямую
app.post("/sql", jsonParser, function (request, response) {
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Headers', 'origin, content-type, accept');

    pool.query("SELECT * FROM `persons`",  async function (err, data) {
        if (err) {
            console.error(err.message)
        }
        else {
            let sendData = await data;
            //console.log(JSON.stringify(data))
            response.send(sendData)
        }
    })
});


//Добавление пользователя
app.post("/create", jsonParser, function (request, response) {
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Headers', 'origin, content-type, accept');

    console.log(request.body)

    const FirstName = request.body.FirstName
    const LastName = request.body.LastName
    const ClassName = request.body.ClassName

    if (request.body != null) {
        pool.query("INSERT INTO persons (FirstName, LastName, ClassName) VALUES (?,?, ?)", [FirstName, LastName, ClassName],async function (err, data) {
            if (err) {
                //console.error(err.message)
                await response.send(err.message)
            }
            else {
                pool.query("SELECT * FROM `persons`", async function (err, data) {
                    if (err) {
                        console.error(err.message)
                    }
                    else {
                        console.log(JSON.stringify(data))
                        response.send(await data)
                    }
                })
            }
        })
    }
});

// получаем id удаляемого пользователя и удаляем его из бд
app.post("/delete",jsonParser, function(req, response){
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Headers', 'origin, content-type, accept');

    const id = req.body.id;
    pool.query("DELETE FROM persons WHERE id=?", [id], async function(err, data) {
        if(err) return await console.log(err);
        else {
            pool.query("SELECT * FROM `persons`", async function (err, data) {
                if (err) {
                    console.error(err.message)
                }
                else {
                    console.log(JSON.stringify(data))
                    response.send(await data)
                }
            })
        }
    });

});

//Изменение полей
app.post("/rename",jsonParser, function(req, response){
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Headers', 'origin, content-type, accept');

    const id = req.body.id;
    pool.query("DELETE FROM persons WHERE id=?", [id], function(err, data) {
        if(err) return console.log(err);
    });
    pool.query("SELECT * FROM `persons`", async function (err, data) {
        if (err) {
            console.error(err.message)
        }
        else {
            console.log(JSON.stringify(data))
            response.send(await data)
        }
    })
});

app.listen(8000, ()=>console.log("Сервер запущен по адресу http://localhost:8000"));